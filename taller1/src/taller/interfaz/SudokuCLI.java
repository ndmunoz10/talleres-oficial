package taller.interfaz;

/*
 * SudokuCLI.java
 * This file is part of SudokuCLI
 *
 * Copyright (C) 2015 - ISIS1206 Team 
 *
 * SudokuCLI is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * SudokuCLI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SudokuCLI. If not, see <http://www.gnu.org/licenses/>.
 */


import taller.mundo.Sudoku;
import static taller.mundo.Sudoku.Rating;

import java.util.Map;
import java.util.Set;
import java.util.Scanner;
import java.util.ArrayList;

/**
 *  La clase <tt>SudokuCLI</tt> establece un sistema de interacciÃ³n directa
 *  con un usuario final, basada en el uso de un entorno de comando de lÃ­nea (CLI).
 *  A travÃ©s de este, un usuario puede acceder y jugar al Sudoku.
 *  @author ISIS1206 Team
 */

public class SudokuCLI
{
	/**
	 * Referencia directa a un juego de Sudoku.
	 **/
	private Sudoku sudoku;

	/**
	 * Interfaz principal al flujo estÃ¡ndar de entrada.
	 * Permite obtener la entrada del usuario a travÃ©s de consola
	 **/
	private Scanner in;

	/**
	 * Constuctor principal de la clase
	 **/
	public SudokuCLI()
	{
		in = new Scanner(System.in);
		sudoku = new Sudoku();
	}

	/**
	 * MenÃº principal del juego
	 **/
	public void mainMenu()
	{
		boolean finish = false;
		while(!finish)
		{
			Screen.clear();
			System.out.println("----------------");
			System.out.println("-              -");
			System.out.println("-    Sudoku    -");
			System.out.println("-              -");
			System.out.println("----------------\n");

			System.out.println("MenÃº principal");
			System.out.println("--------------");
			System.out.println("1. Iniciar nuevo juego");
			System.out.println("2. Salir\n");

			System.out.print("Seleccione una opciÃ³n: ");
			int opt = in.nextInt();

			switch(opt)
			{
			case 1:
				newGame();
				break;
			case 2:
				finish = true;
				break;
			}
			Screen.clear();
		}
	}

	/**
	 * MenÃº de selecciÃ³n de dificultad de una nueva partida de Sudoku
	 **/
	public void newGame()
	{
		sudoku.resetGame();
		in.nextLine();
		Screen.clear();
		System.out.println("Nivel de dificultad");
		System.out.println("-------------------\n");

		Rating[] difficultyLvl = Rating.values();

		/**
		 * TODO: Realice un menÃº de selecciÃ³n de dificultad, de acuerdo a los niveles
		 *       declarados y dispuestos en el arreglo difficultyLvl. Nota: Es posible
		 *       realizar la impresiÃ³n de cada uno de los niveles, a partir de la invocaciÃ³n
		 *       de la funciÃ³n toString(). Adicionalmente, es necesario presentar una opciÃ³n 
		 *       que permita al usuario retornar al menÃº principal. Para este fin, es suficiente
		 *       permitir que el mÃ©todo actual finalice sin ejecutar instrucciones adicionales
		 **/

		System.out.print("Seleccione el nivel de dificultad: \n"
				+ difficultyLvl[0].toString()+ "\n"
				+ difficultyLvl[1].toString() + "\n"
				+ difficultyLvl[2].toString() + "\n"
				+ difficultyLvl[3].toString() + "\n"
				+ difficultyLvl[4].toString() + "\n" +
				"Salir para regresar el menu principal: ");
		String nivel = in.nextLine();
		if(nivel.equals(difficultyLvl[0].toString()))
		{
			sudoku.setUpGame(difficultyLvl[0]);
		}
		if(nivel.equals(difficultyLvl[1].toString()))
		{
			sudoku.setUpGame(difficultyLvl[1]);
		}
		if(nivel.equals(difficultyLvl[2].toString()))
		{
			sudoku.setUpGame(difficultyLvl[2]);
		}
		if(nivel.equals(difficultyLvl[3].toString()))
		{
			sudoku.setUpGame(difficultyLvl[3]);
		}
		if(nivel.equals(difficultyLvl[4].toString()))
		{
			sudoku.setUpGame(difficultyLvl[4]);
		}
		if(nivel.equalsIgnoreCase("Salir"))
		{
			mainMenu();
		}
		startGame();
	}

	/**
	 * Ciclo principal de ejecuciÃ³n del juego de Sudoku
	 **/
	public void startGame()
	{
		/**
		 * TODO: Construya la interfaz de interacciÃ³n principal con la partida en curso.
		 *       Esta interfaz debe, en primer lugar, visualizar el estado actual del
		 *       tablero de juego (Ver printBoard). En segundo lugar, debe ofrecer un menÃº
		 *       bajo el cual, el usuario pueda realizar las siguientes acciones:
		 *           - Introducir un valor en una casilla
		 *           - Eliminar el valor de una casilla
		 *           - Finalizar juego y ver soluciÃ³n
		 *       Finalmente, es necesario verificar el estado de finalizaciÃ³n de juego
		 *       tras la ejecuciÃ³n de un cambio en el tablero (Ver Sudoku.hasGameFinished())
		 **/
		boolean fin = sudoku.hasGameFinished();
		while (!fin)
		{
			System.out.println(printBoard(sudoku.getBoard()));
			System.out.println("Escriba 1 para introducir un valor en una casilla. \n"
					+ "Escriba 2 para eliminar un valor de una casilla. \n"
					+ "Escriba 3 para finalizar el juego y ver la solución.");

			int opcion = in.nextInt();
			if(opcion == 1)
			{
				enterValue();
			}
			else if(opcion == 2)
			{
				deleteValue();
			}
			else if(opcion == 3)
			{
				Screen.clear();
				in.nextLine();
				System.out.println("SoluciÃ³n posible al juego");
				System.out.println("-------------------------");
				System.out.println(printBoard(sudoku.getSolution()));
				fin = true;
			}
		}
		
		System.out.print("Presione cualquier tecla para continuar...");
		in.nextLine();

	}

	/**
	 * Dialogo dispuesto para la sustituciÃ³n de un valor en el tablero de sudoku.
	 * Si el usuario ingresa un valor incorrecto, o si el valor ingresado viola alguna
	 * de las restricciones del juego, se informarÃ¡ al mismo a travÃ©s de consola.
	 **/
	public void enterValue()
	{
		in.nextLine();
		System.out.println("\nIntroduzca la coordenada de la casilla, acompaÃ±ada del nÃºmero a introducir, e.g., A2-3\n");
		System.out.print("Casilla: ");

		String coor = in.nextLine();
		String[] values = coor.split("-");
		int num = Integer.parseInt(values[1]);
		int row = ((int) values[0].charAt(0)) - 65;
		int col = Integer.parseInt(""+values[0].charAt(1))-1;
		if(row < 0 || row > 8)
		{
			System.out.println("La coordenada de la fila debe encontrarse en el rango A-I.");
		}
		else
		{
			if(col < 0 || col > 8)
			{
				System.out.println("La coordenada de la columna debe encontrarse en el rango 1-9.");
			}
			else
			{
				String result = sudoku.replaceValue(row, col, num, false);
				System.out.println("\n"+result);
			}
		}
		System.out.print("Presione cualquier tecla para continuar...");
		in.nextLine();
	}

	/**
	 * Dialogo dispuesto para la eliminaciÃ³n de un valor en el tablero de sudoku.
	 * Si el usuario ingresa un valor incorrecto, o si el valor ingresado viola alguna
	 * de las restricciones del juego, se informarÃ¡ al mismo a travÃ©s de consola.
	 **/
	public void deleteValue()
	{
		in.nextLine();
		System.out.println("\nIntroduzca la coordenada de la casilla que se desea eliminar, e.g., B4\n");
		System.out.print("Casilla: ");

		String coor = in.nextLine();
		int row = ((int) coor.charAt(0)) - 65;
		int col = Integer.parseInt(""+coor.charAt(1))-1;
		if(row < 0 || row > 8)
		{
			System.out.println("La coordenada de la fila debe encontrarse en el rango A-I.");
		}
		else
		{
			if(col < 0 || col > 8)
			{
				System.out.println("La coordenada de la columna debe encontrarse en el rango 1-9.");
			}
			else
			{
				String result = sudoku.replaceValue(row, col, 0, true);
				System.out.println("\n"+result);
			}
		}
		System.out.print("Presione cualquier tecla para continuar...");
		in.nextLine();
	}

	/**
	 * Imprime un tablero de Sudoku en consola, a partir del uso de caractÃ©res de construcciÃ³n de tablas.
	 * @param Matriz que describe un tablero de un juego de Sudoku.
	 * Cada entrada de la matriz, contiene un nÃºmero entre 1 y 9 si Ã©sta no se
	 * encuentra vacÃ­a. 0 de lo contrario
	 * @return RepresentaciÃ³n textual de un tablero de sudoku.
	 **/
	public String printBoard(int[][] board)
	{
		StringBuilder sb = new StringBuilder();
		sb.append("\n     1   2   3   4   5   6   7   8   9\n");
		sb.append("   ┌───┬───┬───┬───┬───┬───┬───┬───┬───┐\n");
		int rowNum = 1;
		for(int[] row : board)
		{
			sb.append(" "+((char)(rowNum + 64))+" ");
			for(int col : row)
			{
				String val = (col == 0) ? " " : ""+col;
				sb.append("│ "+val+" ");
			}
			sb.append("│\n");
			if(rowNum != 9)
			{
				sb.append("   ├───┼───┼───┼───┼───┼───┼───┼───┼───┤\n");
			}
			else
			{
				sb.append("   └───┴───┴───┴───┴───┴───┴───┴───┴───┘\n");
			}
			rowNum++;
		}
		return sb.toString();
	}


}

